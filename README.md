# minimal-lambda-python-terraform

## Create the python function

Note: For this example we are naming the file: `lambda.py` ,
which is important to sync up with the terraform setup:

``` python
import json

def handler(event, context):
    print("Received event: " + json.dumps(event, indent=2))
```

## Package as a zip

``` bash
zip lambda lambda.py
```

## Define the terraform setup

In a file named anything ending with .tf*
For Example: `main.tf`

``` terraform

provider "aws" {
  region = "us-east-1"
}

variable "function_name" {
  default = "minimal_lambda_function"
}

variable "handler" {
  default = "lambda.handler"
}

variable "runtime" {
  default = "python3.6"
}

resource "aws_lambda_function" "lambda_function" {
  role             = "${aws_iam_role.lambda_exec_role.arn}"
  handler          = "${var.handler}"
  runtime          = "${var.runtime}"
  filename         = "lambda.zip"
  function_name    = "${var.function_name}"
  source_code_hash = "${base64sha256(file("lambda.zip"))}"
}

resource "aws_iam_role" "lambda_exec_role" {
  name        = "lambda_exec"
  path        = "/"
  description = "Allows Lambda Function to call AWS services on your behalf."

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}
```

## Create the lambda

``` bash
terraform apply
```

## Invoke the lambda

``` bash
aws lambda invoke --invocation-type RequestResponse --function-name minimal_lambda_function --region us-east-1 --log-type Tail --payload '{"key1":"value1", "key2":"value2", "key3":"value3"}' outputfile.txt | jq .LogResult | sed 's/"//g' | base64 --decode
```

## Resources

* Original source: [The most minimal AWS Lambda + Python + Terraform setup](https://www.davidbegin.com/the-most-minimal-aws-lambda-function-with-python-terraform/)
* [Hashicorp Learn - Terraform - Get Started - AWS](https://learn.hashicorp.com/collections/terraform/aws-get-started)
* [express42/terraform-aws-lambda-python](https://github.com/express42/terraform-aws-lambda-python)
